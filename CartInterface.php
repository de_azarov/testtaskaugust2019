<?php declare(strict_types=1);

namespace App;

/**
 * @author Denis Azarov <denis@azarov.de>
 */
interface CartInterface
{
    const VAT_PERCENTAGE = 18;

    /**
     * @return float
     */
    public function getItemsSubtotal(): float;

    /**
     * @return void
     */
    public function setItemsSubtotal(): void;

    /**
     * @return integer
     */
    public function getDiscountPercentage(): int;

    /**
     * @param int $discountPercentage
     *
     * @return void
     */
    public function setDiscountPercentage(int $discountPercentage): void;

    /**
     * @return float
     */
    public function getDiscountAbsolute(): float;

    /**
     * @return void
     */
    public function setDiscountAbsolute(): void;

    /**
     * @return float
     */
    public function getVAT(): float;

    /**
     * @return void
     */
    public function setVAT(): void;

    /**
     * @return float
     */
    public function getTotal(): float;

    /**
     * @return void
     */
    public function setTotal(): void;

    /**
     * @return void
     */
    public function notifyByEmail(): void;

    /**
     * @return void
     */
    public function makeOrder(): void;
}
