<?php declare(strict_types=1);

namespace App;

/**
 * @author Denis Azarov <denis@azarov.de>
 */
class Cart implements CartInterface
{
    /**
     * @var UserInterface|null
     */
    private $user;

    /**
     * @var array
     */
    private $items;

    /**
     * @var OrderInterface|null
     */
    private $order;

    /**
     * @var float
     */
    private $itemsSubtotal;

    /**
     * @var integer
     */
    private $discountPercentage;

    /**
     * @var float
     */
    private $discountAbsolute;

    /**
     * @var integer
     */
    private $VAT;

    /**
     * @var float
     */
    private $total;

    /**
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
        $this->items = [];
        $this->itemsSubtotal = 0;
        $this->discountPercentage = 0;
        $this->discountAbsolute = 0;
        $this->VAT = 0;
        $this->total = 0;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->getUser();
    }

    /**
     * @return OrderInterface|null
     */
    public function getOrder(): ?OrderInterface
    {
        return $this->order;
    }

    /**
     * @param OrderInterface $order
     */
    public function setOrder(OrderInterface $order): void
    {
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function getItemsSubtotal(): float
    {
        return $this->getItemsSubtotal();
    }

    /**
     * @return void
     */
    public function setItemsSubtotal(): void
    {
        $subtotal = 0;

        foreach ($this->items as $item) {
            $subtotal += $item->getPrice();
        }

        $this->itemsSubtotal = $subtotal;
    }

    /**
     * @return integer
     */
    public function getDiscountPercentage(): int
    {
        return $this->getDiscountPercentage();
    }

    /**
     * @param integer $discountPercentage
     */
    public function setDiscountPercentage(int $discountPercentage): void
    {
        $this->discountPercentage = $discountPercentage;
    }

    /**
     * @return float
     */
    public function getDiscountAbsolute(): float
    {
        return $this->getDiscountAbsolute();
    }

    /**
     * @return void
     */
    public function setDiscountAbsolute(): void
    {
        $this->discountAbsolute = $this->getItemsSubtotal() * ($this->getDiscountPercentage() / 100);
    }

    /**
     * @return integer
     */
    public function getVAT(): int
    {
        return $this->getVAT();
    }

    /**
     * @return void
     */
    public function setVAT(): void
    {
        $this->VAT = ($this->getItemsSubtotal() - $this->getDiscountAbsolute()) * (self::VAT_PERCENTAGE / 100);
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->getTotal();
    }

    /**
     * @return void
     */
    public function setTotal(): void
    {
        $this->total = $this->getItemsSubtotal() - $this->getDiscountAbsolute() + $this->getVAT();
    }

    /**
     * @return void
     */
    public function notifyByEmail(): void
    {
        $this->sendMail();
    }

    /**
     * @return void
     */
    public function makeOrder(): void
    {
        $this->order = new Order($this->items, $this->getTotal());
        $this->sendMail();
    }

    /**
     * @return void
     */
    public function sendMail(): void
    {
        $mailer = new SimpleMailer($this->getUser()->getEmail());
        $mailMessage = sprintf(
            "<p>Оформлен новый заказ №<b>%d</b> на сумму %f руб.</p>",
            $this->getOrder()->getId(),
            $this->getTotal()
        );
        $mailer->sendToManagers($mailMessage);
    }
}